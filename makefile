# Copyright (C) 2021  Stephan Kreutzer
#
# This file is part of JTeroStA.
#
# JTeroStA is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# JTeroStA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.



.PHONY: all JTeroStA clean



all: JTeroStA
JTeroStA: JTeroStA.class



org/publishing_systems/_20140527t120137z/jterosta/JTeroException.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroException.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroException.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamInterface.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamInterface.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamInterface.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamStd.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamStd.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamStd.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroRule.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroRule.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroRule.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroCharacter.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroCharacter.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroCharacter.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroSequence.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroSequence.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroSequence.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroPattern.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroPattern.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroPattern.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroThenCase.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroThenCase.java org/publishing_systems/_20140527t120137z/jterosta/JTeroPattern.class
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroThenCase.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroFunction.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroFunction.java org/publishing_systems/_20140527t120137z/jterosta/JTeroThenCase.class
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroFunction.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroLoader.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroLoader.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroLoader.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroEvent.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroEvent.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroEvent.java

org/publishing_systems/_20140527t120137z/jterosta/JTeroStAInterpreter.class: org/publishing_systems/_20140527t120137z/jterosta/JTeroStAInterpreter.java
	javac -encoding UTF-8 org/publishing_systems/_20140527t120137z/jterosta/JTeroStAInterpreter.java

JTeroStA.class: JTeroStA.java org/publishing_systems/_20140527t120137z/jterosta/JTeroException.class org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamInterface.class org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamStd.class org/publishing_systems/_20140527t120137z/jterosta/JTeroRule.class org/publishing_systems/_20140527t120137z/jterosta/JTeroCharacter.class org/publishing_systems/_20140527t120137z/jterosta/JTeroSequence.class org/publishing_systems/_20140527t120137z/jterosta/JTeroPattern.class org/publishing_systems/_20140527t120137z/jterosta/JTeroThenCase.class org/publishing_systems/_20140527t120137z/jterosta/JTeroFunction.class org/publishing_systems/_20140527t120137z/jterosta/JTeroLoader.class org/publishing_systems/_20140527t120137z/jterosta/JTeroEvent.class org/publishing_systems/_20140527t120137z/jterosta/JTeroStAInterpreter.class
	javac -encoding UTF-8 JTeroStA.java

clean:
	rm -f JTeroStA.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroStAInterpreter.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroEvent.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroLoader.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroFunction.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroThenCase.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroPattern.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroSequence.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroCharacter.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroRule.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamStd.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamInterface.class
	rm -f org/publishing_systems/_20140527t120137z/jterosta/JTeroException.class
