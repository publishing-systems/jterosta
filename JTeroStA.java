/* Copyright (C) 2021-2023  Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JTeroStA.java
 * @brief Demo program.
 * @author Stephan Kreutzer
 * @since 2021-11-11
 */



import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Map;
import org.publishing_systems._20140527t120137z.jterosta.JTeroLoader;
import org.publishing_systems._20140527t120137z.jterosta.JTeroPattern;
import org.publishing_systems._20140527t120137z.jterosta.JTeroFunction;
import org.publishing_systems._20140527t120137z.jterosta.JTeroStAInterpreter;
import org.publishing_systems._20140527t120137z.jterosta.JTeroEvent;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamInterface;
import org.publishing_systems._20140527t120137z.jterosta.JTeroInputStreamStd;
import org.publishing_systems._20140527t120137z.jterosta.JTeroException;



public class JTeroStA
{
    public static void main(String args[])
    {
        System.out.print("JTeroStA Copyright (C) 2021-2023 Stephan Kreutzer\n" +
                         "This program comes with ABSOLUTELY NO WARRANTY.\n" +
                         "This is free software, and you are welcome to redistribute it\n" +
                         "under certain conditions. See the GNU Affero General Public License 3\n" +
                         "or any later version for details. Also, see the source code repository\n" +
                         "https://gitlab.com/publishing-systems/JTeroStA/ and\n" +
                         "the project website https://publishing-systems.org.\n\n");

        try
        {
            if (args.length >= 1)
            {
                File inputFile = new File(args[0]);

                if (inputFile.exists() != true)
                {
                    System.out.println("File '" + args[0] + "' doesn't exist.");
                    return;
                }

                if (inputFile.isFile() != true)
                {
                    System.out.println("Path '" + args[0] + "' isn't a file.");
                    return;
                }

                if (inputFile.canRead() != true)
                {
                    System.out.println("File '" + args[0] + "' isn't readable.");
                    return;
                }

                InputStream in = new FileInputStream(inputFile);

                Run(in);
            }
            else
            {
                System.out.print("Usage:\n\n\tJTeroStA <input-html-file>\n\n");
            }
        }
        catch (Exception ex)
        {
            System.out.println("Exception: " + ex.getMessage());
            return;
        }
    }

    public static int Run(InputStream inputStream) throws JTeroException
    {
        JTeroInputStreamInterface stream = new JTeroInputStreamStd(inputStream);

        JTeroLoader teroLoader = new JTeroLoader("./Html/");
        teroLoader.load();

        Map<String, JTeroPattern> patterns = teroLoader.getPatterns();
        Map<String, JTeroFunction> functions = teroLoader.getFlow();

        JTeroStAInterpreter interpreter = new JTeroStAInterpreter(functions, patterns, "InMain", stream, false);

        boolean isInParagraph = false;
        boolean isElementEnd = false;

        while (interpreter.hasNext() == true)
        {
            JTeroEvent event = interpreter.nextEvent();

            if (event.getCurrentFunctionName().equals("InHtmlTagStart") == true)
            {
                if (event.getNextFunctionPatternName() != null)
                {
                    if (event.getNextFunctionPatternName().equals("HtmlElementEndMarker") == true)
                    {
                        isElementEnd = true;
                    }
                }
                else
                {
                    /** @todo What if absence of pattern match (else-case) bears meaning? */
                }
            }
            else if (event.getCurrentFunctionName().equals("InHtmlElementName") == true)
            {
                if (event.getData().equals("p") == true)
                {
                    isInParagraph = !(isElementEnd);

                    if (isInParagraph != true)
                    {
                        System.out.print("\n\n");
                    }
                }
            }
            else
            {
                isElementEnd = false;

                if (isInParagraph == true &&
                    event.getCurrentFunctionName().equals("InMain") == true)
                {
                    System.out.print(event.getData());
                }
            }
        }

        return 0;
    }
}
