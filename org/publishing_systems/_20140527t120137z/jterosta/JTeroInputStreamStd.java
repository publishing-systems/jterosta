/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamStd.java
 * @author Stephan Kreutzer
 * @since 2021-11-06
 */

package org.publishing_systems._20140527t120137z.jterosta;



import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.io.IOException;



public class JTeroInputStreamStd implements JTeroInputStreamInterface
{
    public JTeroInputStreamStd(InputStream stream) throws JTeroException
    {
        if (stream == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        try
        {
            this.stream = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        }
        catch (UnsupportedEncodingException ex)
        {
            this.stream = null;
            throw new JTeroException(ex);
        }
    }

    public char get() throws JTeroException
    {
        if (this.eof == true)
        {
            return '\0';
        }

        if (this.bufferIndex < 0)
        {
            int character = -1;

            try
            {
                character = this.stream.read();
            }
            catch (IOException ex)
            {
                throw new JTeroException(ex);
            }

            if (character < 0)
            {
                this.eof = true;
                return '\0';
            }
            else
            {
                return (char)character;
            }
        }
        else
        {
            char character = this.buffer.charAt(this.bufferIndex);
            ++this.bufferIndex;

            if (this.bufferIndex >= this.buffer.length())
            {
                this.buffer = "";
                this.bufferIndex = -1;
            }

            return character;
        }
    }

    public boolean eof()
    {
        return this.eof;
    }

    public int push(String addition)
    {
        if (this.eof == true)
        {
            return -1;
        }

        if (addition.isEmpty() == true)
        {
            return 0;
        }

        this.buffer += addition;

        if (this.bufferIndex < 0)
        {
            this.bufferIndex = 0;
        }

        return 0;
    }

    protected BufferedReader stream = null;
    protected String buffer = "";
    protected int bufferIndex = -1;

    protected boolean eof = false;
}
