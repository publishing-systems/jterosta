/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jterosta/JTeroCharacter.java
 * @author Stephan Kreutzer
 * @since 2021-10-14
 */

package org.publishing_systems._20140527t120137z.jterosta;



public class JTeroCharacter implements JTeroRule
{
    public JTeroCharacter(char character)
    {
        this.character = character;
    }

    public int compare(char character)
    {
        if (this.character == character)
        {
            return JTeroRule.RETURNVALUE_COMPARE_MATCH;
        }
        else
        {
            return JTeroRule.RETURNVALUE_COMPARE_MISMATCH;
        }
    }

    public int reset()
    {
        return 0;
    }

    public String getSequence()
    {
        return String.valueOf(this.character);
    }

    protected char character = '\0';
}
