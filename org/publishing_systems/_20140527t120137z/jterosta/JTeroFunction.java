/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jterosta/JTeroFunction.java
 * @author Stephan Kreutzer
 * @since 2021-10-15
 */

package org.publishing_systems._20140527t120137z.jterosta;



import java.util.List;



public class JTeroFunction
{
    public JTeroFunction(String name,
                         List<JTeroThenCase> thenCases,
                         String elseCaseCode,
                         String elseCaseFunction,
                         boolean hasRetain)
    {
        this.name = name;
        this.thenCases = thenCases;
        this.elseCaseCode = elseCaseCode;
        this.elseCaseFunction = elseCaseFunction;
        this.hasRetain = hasRetain;
    }

    public String getName()
    {
        return this.name;
    }

    public List<JTeroThenCase> getThenCases()
    {
        return this.thenCases;
    }

    public String getElseCaseCode()
    {
        return this.elseCaseCode;
    }

    public String getElseCaseFunction()
    {
        return this.elseCaseFunction;
    }

    public boolean getHasRetain()
    {
        return this.hasRetain;
    }

    protected String name = null;
    protected List<JTeroThenCase> thenCases = null;
    protected String elseCaseCode = null;
    protected String elseCaseFunction = null;
    protected boolean hasRetain = false;
}
