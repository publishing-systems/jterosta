/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jterosta/JTeroInputStreamInterface.java
 * @author Stephan Kreutzer
 * @since 2021-11-06
 */

package org.publishing_systems._20140527t120137z.jterosta;



public interface JTeroInputStreamInterface
{
    char get() throws JTeroException;
    boolean eof();

    int push(String buffer);
}
