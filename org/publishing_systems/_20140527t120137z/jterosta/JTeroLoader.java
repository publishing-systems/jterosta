/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jterosta/JTeroLoader.java
 * @author Stephan Kreutzer
 * @since 2021-10-14
 */

package org.publishing_systems._20140527t120137z.jterosta;



import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;



public class JTeroLoader
{
    public JTeroLoader(String codeDirectoryPath) throws JTeroException
    {
        File codeDirectory = new File(codeDirectoryPath);

        if (codeDirectory.exists() != true)
        {
            throw new JTeroException("Code directory \"" + codeDirectory.getAbsolutePath() + "\" doesn't exist.");
        }

        if (codeDirectory.isDirectory() != true)
        {
            throw new JTeroException("Code path \"" + codeDirectory.getAbsolutePath() + "\" isn't a directory.");
        }

        if (codeDirectory.canRead() != true)
        {
            throw new JTeroException("Code directory \"" + codeDirectory.getAbsolutePath() + "\" isn't readable.");
        }

        this.codeDirectory = codeDirectory;
    }

    public int load() throws JTeroException
    {
        return load(false);
    }

    public int load(boolean tolerateUnresolvableFunctionReferences) throws JTeroException
    {
        this.patterns = loadPatterns();
        this.functions = loadFunctions(tolerateUnresolvableFunctionReferences);

        return 0;
    }

    public Map<String, JTeroPattern> getPatterns() throws JTeroException
    {
        if (this.patterns == null)
        {
            throw new JTeroException(new IllegalStateException());
        }

        return this.patterns;
    }

    public Map<String, JTeroFunction> getFlow() throws JTeroException
    {
        // "Flow" is user-facing terminology. Internally, it's "functions".

        if (this.functions == null)
        {
            throw new JTeroException(new IllegalStateException());
        }

        return this.functions;
    }

    protected Map<String, JTeroPattern> loadPatterns() throws JTeroException
    {
        File patternsDirectory = new File(this.codeDirectory.getAbsolutePath() + File.separator + "patterns");

        if (patternsDirectory.exists() != true)
        {
            throw new JTeroException("Patterns directory \"" + patternsDirectory.getAbsolutePath() + "\" doesn't exist.");
        }

        if (patternsDirectory.isDirectory() != true)
        {
            throw new JTeroException("Patterns path \"" + patternsDirectory.getAbsolutePath() + "\" isn't a directory.");
        }

        if (patternsDirectory.canRead() != true)
        {
            throw new JTeroException("Patterns directory \"" + patternsDirectory.getAbsolutePath() + "\" isn't readable.");
        }

        Map<String, JTeroPattern> patterns = new HashMap<String, JTeroPattern>();
        String codeFileNames[] = patternsDirectory.list();

        for (int i = 0, max = codeFileNames.length; i < max; i++)
        {
            if (codeFileNames[i].endsWith(".teroptrn") != true)
            {
                continue;
            }

            // Could also easily calculate length - ".teroptrn".length.
            String patternName = codeFileNames[i].substring(0, codeFileNames[i].lastIndexOf(".teroptrn"));

            AbstractMap.SimpleEntry<List<String>, Boolean> loadResult = loadSections(patternsDirectory.getAbsolutePath() + File.separator + codeFileNames[i]);
            List<String> sections = loadResult.getKey();

            int sectionCount = sections.size();

            if (sectionCount < 2)
            {
                throw new JTeroException("Less than the minimum total of at least 2 sections.");
            }

            if (sections.get(0).isEmpty() == true)
            {
                throw new JTeroException("Section 1 is empty.");
            }

            if (sections.get(0).equals(patternName) != true)
            {
                throw new JTeroException("Pattern name \"" + sections.get(0) + "\" does mismatch expected pattern file name \"" + patternName + "\" of path \"" + codeFileNames[i] + "\".");
            }

            if (patterns.containsKey(patternName) == true)
            {
                throw new JTeroException("Pattern \"" + patternName + "\" defined more than once.");
            }

            List<JTeroRule> rules = new ArrayList<JTeroRule>();
            /** @todo This is just to check for duplicates. Better replace by efficient rules list lookup? */
            List<String> sectionsUnique = new ArrayList<String>();

            for (int j = 1; j < sectionCount; j++)
            {
                String section = sections.get(j);

                if (section.isEmpty() == true)
                {
                    throw new JTeroException("In pattern \"" + sections.get(0) + "\", section/sequence " + j + " is empty.");
                }

                if (sectionsUnique.contains(section) != true)
                {
                    sectionsUnique.add(section);
                }
                else
                {
                    throw new JTeroException("In pattern \"" + sections.get(0) + "\", sequence \"" + section + "\" is defined more than once.");
                }

                int sectionLength = section.length();

                if (sectionLength == 1)
                {
                    rules.add(new JTeroCharacter(section.charAt(0)));
                }
                else if (sectionLength > 1)
                {
                    rules.add(new JTeroSequence(section));
                }
                else
                {
                    throw new JTeroException("In pattern \"" + sections.get(0) + "\", section/sequence \"" + j + "\" is empty.");
                }
            }

            patterns.put(patternName, new JTeroPattern(patternName, rules));
        }

        return patterns;
    }

    protected Map<String, JTeroFunction> loadFunctions(boolean tolerateUnresolvableFunctionReferences) throws JTeroException
    {
        if (this.patterns == null)
        {
            this.patterns = loadPatterns();
        }

        if (this.patterns == null)
        {
            throw new JTeroException(new IllegalStateException());
        }

        File flowDirectory = new File(this.codeDirectory.getAbsolutePath() + File.separator + "flow");

        if (flowDirectory.exists() != true)
        {
            throw new JTeroException("Flow directory \"" + flowDirectory.getAbsolutePath() + "\" doesn't exist.");
        }

        if (flowDirectory.isDirectory() != true)
        {
            throw new JTeroException("Flow path \"" + flowDirectory.getAbsolutePath() + "\" isn't a directory.");
        }

        if (flowDirectory.canRead() != true)
        {
            throw new JTeroException("Flow directory \"" + flowDirectory.getAbsolutePath() + "\" isn't readable.");
        }

        Map<String, JTeroFunction> functions = new HashMap<String, JTeroFunction>();
        String codeFileNames[] = flowDirectory.list();

        for (int i = 0, max = codeFileNames.length; i < max; i++)
        {
            if (codeFileNames[i].endsWith(".teroflow") != true)
            {
                continue;
            }

            // Could also easily calculate length - ".teroflow".length.
            String functionName = codeFileNames[i].substring(0, codeFileNames[i].lastIndexOf(".teroflow"));

            AbstractMap.SimpleEntry<List<String>, Boolean> loadResult = loadSections(flowDirectory.getAbsolutePath() + File.separator + codeFileNames[i]);
            List<String> sections = loadResult.getKey();

            int sectionCount = sections.size();

            if (sectionCount < 6)
            {
                throw new JTeroException("Less than the minimum total of at least 6 sections.");
            }

            {
                int sectionCountThenCases = (sectionCount - 3);

                if (sectionCountThenCases % 3 != 0)
                {
                    throw new JTeroException("Then-case incomplete, section(s) missing.");
                }
            }

            if (sections.get(0).isEmpty() == true)
            {
                throw new JTeroException("Section 1 is empty.");
            }

            if (sections.get(0).equals(functionName) != true)
            {
                throw new JTeroException("Function name \"" + sections.get(0) + "\" does mismatch expected code file name \"" + functionName + "\" of path \"" + codeFileNames[i] + "\".");
            }

            if (functions.containsKey(functionName) == true)
            {
                throw new JTeroException("Function \"" + functionName + "\" defined more than once.");
            }

            List<JTeroThenCase> thenCases = new ArrayList<JTeroThenCase>();
            /** @todo This is just to check for duplicates. Better replace by efficient thenCases list lookup? */
            List<String> thenCasesUnique = new ArrayList<String>();

            for (int j = 1, max2 = (sectionCount - 2); j < max2; j += 3)
            {
                String section = sections.get(j);

                if (section.isEmpty() == true)
                {
                    throw new JTeroException("In function \"" + sections.get(0) + "\", section/sequence " + j + " is empty.");
                }

                if (thenCasesUnique.contains(section) != true)
                {
                    thenCasesUnique.add(section);
                }
                else
                {
                    throw new JTeroException("In function \"" + sections.get(0) + "\", sequence \"" + section + "\" is defined more than once.");
                }

                if (sections.get(j + 2).isEmpty() == true)
                {
                    throw new JTeroException("In function \"" + sections.get(0) + "\", section " + (j + 2) + " is empty.");
                }

                // In theory, would support multiple patterns for a then-case, but notation
                // currently supports only one, probably for the better (is cleaner?).
                List<JTeroPattern> patternList = new ArrayList<JTeroPattern>();

                if (this.patterns.containsKey(sections.get(j)) != true)
                {
                    throw new JTeroException("In function \"" + sections.get(0) + "\", use of unknown pattern \"" + sections.get(j) + "\".");
                }

                patternList.add(this.patterns.get(sections.get(j)));

                thenCases.add(new JTeroThenCase(patternList,
                                                sections.get(j + 1),
                                                sections.get(j + 2)));
            }

            if (sections.get(sectionCount - 1).isEmpty() == true)
            {
                throw new JTeroException("In function \"" + sections.get(0) + "\", section " + (sectionCount - 1) + " is empty.");
            }

            JTeroFunction function = new JTeroFunction(sections.get(0),
                                                       thenCases,
                                                       sections.get(sectionCount - 2),
                                                       sections.get(sectionCount - 1),
                                                       loadResult.getValue());

            functions.put(sections.get(0), function);
        }

        if (tolerateUnresolvableFunctionReferences != true)
        {
            for (Map.Entry<String, JTeroFunction> function : functions.entrySet())
            {
                List<JTeroThenCase> thenCases = function.getValue().getThenCases();

                for (int i = 0, max = thenCases.size(); i < max; i++)
                {
                    if (functions.containsKey(thenCases.get(i).getThenCaseFunction()) != true)
                    {
                        throw new JTeroException("In function \"" + function.getValue().getName() + "\", use of unknown function \"" + thenCases.get(i).getThenCaseFunction() + "\".");
                    }
                }
            }
        }

        return functions;
    }

    protected AbstractMap.SimpleEntry<List<String>, Boolean> loadSections(String inputFilePath) throws JTeroException
    {
        File inputFile = new File(inputFilePath);

        if (inputFile.exists() != true)
        {
            throw new JTeroException("Tero input file \"" + inputFile.getAbsolutePath() + "\" doesn't exist.");
        }

        if (inputFile.isFile() != true)
        {
            throw new JTeroException("Tero input path \"" + inputFile.getAbsolutePath() + "\" isn't a file.");
        }

        if (inputFile.canRead() != true)
        {
            throw new JTeroException("Tero input file \"" + inputFile.getAbsolutePath() + "\" isn't readable.");
        }

        List<String> sections = new ArrayList<String>();
        boolean hasRetain = false;

        try
        {
            BufferedReader reader = new BufferedReader(
                                    new InputStreamReader(
                                    new FileInputStream(inputFile),
                                    "UTF8"));

            int character = reader.read();

            while (character != -1)
            {
                if (character == '(')
                {
                    sections.add(handleSection(reader, ')'));
                }
                else if (character == '[')
                {
                    sections.add(handleSection(reader, ']'));
                    hasRetain = true;
                }

                character = reader.read();
            }

            reader.close();
        }
        catch (FileNotFoundException ex)
        {
            throw new JTeroException(ex);
        }
        catch (UnsupportedEncodingException ex)
        {
            throw new JTeroException(ex);
        }
        catch (IOException ex)
        {
            throw new JTeroException(ex);
        }

        return new AbstractMap.SimpleEntry<List<String>, Boolean>(sections, hasRetain);
    }

    protected String handleSection(BufferedReader reader, int endCharacter) throws IOException, JTeroException
    {
        StringBuilder sb = new StringBuilder();

        int character = reader.read();

        do
        {
            if (character == -1)
            {
                throw new JTeroException("Premature end of file while reading sections.");
            }

            if (character == endCharacter)
            {
                break;
            }
            else
            {
                sb.append((char)character);
            }

            character = reader.read();

        } while (true);

        return sb.toString();
    }

    protected File codeDirectory = null;
    protected Map<String, JTeroPattern> patterns = null;
    protected Map<String, JTeroFunction> functions = null;
}
