/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of JTeroStA.
 *
 * JTeroStA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or any later version,
 * as published by the Free Software Foundation.
 *
 * JTeroStA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with JTeroStA. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/org/publishing_systems/_20140527t120137z/jterosta/JTeroPattern.java
 * @author Stephan Kreutzer
 * @since 2021-10-14
 */

package org.publishing_systems._20140527t120137z.jterosta;



import java.util.List;



public class JTeroPattern
{
    public JTeroPattern(String name, List<JTeroRule> rules) throws JTeroException
    {
        if (name == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        if (rules == null)
        {
            throw new JTeroException(new IllegalArgumentException());
        }

        this.name = name;
        this.rules = rules;
    }

    public String getName()
    {
        return this.name;
    }

    public List<JTeroRule> getRules()
    {
        return this.rules;
    }

    protected String name = null;
    List<JTeroRule> rules = null;
}
